# import logging
import os
from typing import Annotated, Optional

import vtk
import qt
import sys

moduleDir = os.path.dirname(__file__)
sys.path.append(moduleDir)

import slicer
from slicer.i18n import tr as _
from slicer.i18n import translate
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin
from slicer.parameterNodeWrapper import (
    parameterNodeWrapper,
    WithinRange,
)

from slicer import vtkMRMLScalarVolumeNode

from DICOMLib import DICOMUtils

import shutil
import re
import time
from modules.AffineMatrix import AffineMatrix
import numpy as np


from epiSTIM.core.PatientModelEpilepsy import PatientModelEpilepsy




























#
# DataManagement
#


class DataManagement(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = _("Data Management")
        self.parent.categories = ["Epimods"]
        self.parent.dependencies = []
        self.parent.contributors = ["Martin Gregorio"]
        self.parent.helpText = _("Import module for STIM. See more information in <a href='https://gitlab.com/icm-institute/STIM/epiSTIM'>epiSTIM src</a>.")
        self.parent.acknowledgementText = _("This file was originally developed by Martin Gregorio and wraps some epiSTIM scripts written by Sara FERNANDEZ VIDAL")



#
# Register sample data sets in Sample Data module
#




























#
# DataManagementParameterNode
#


@parameterNodeWrapper
class DataManagementParameterNode:
    pass


#
# DataManagementWidget
#


class DataManagementWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    newSubjectText = "New Subject"

    def __init__(self, parent=None) -> None:
        """Called when the user opens the module the first time and the widget is initialized."""
        ScriptedLoadableModuleWidget.__init__(self, parent)
        VTKObservationMixin.__init__(self)  # needed for parameter node observation
        self.logic = None
        self._parameterNode = None
        self._parameterNodeGuiTag = None


        self.historyDir = os.path.join(moduleDir, 'History')
        self.historyPath = os.path.join(self.historyDir, 'history.txt')


    def setup(self) -> None:
        """Called when the user opens the module the first time and the widget is initialized."""
        ScriptedLoadableModuleWidget.setup(self)


        # Create logic class
        self.logic = DataManagementLogic()


        # Create UI
        uiWidget = slicer.util.loadUI(self.resourcePath("UI/DataManagement.ui"))
        self.layout.addWidget(uiWidget)
        self.ui = slicer.util.childWidgetVariables(uiWidget)
        self.groupBoxes = [self.ui.import_groupbox, self.ui.actions_groupbox]

        for box in self.groupBoxes:
            box.setEnabled(False)
            box.setChecked(False)
        
        self.ui.seqs_section_button.setChecked(False)
        self.ui.seqs_section_button.setEnabled(False)






        self.ui.choose_folder_button.clicked.connect(self.requestWorkingDirectory)
        self.ui.open_button.clicked.connect(self.chooseSubject)

        self.ui.choose_DICOM_folder_button.clicked.connect(self.importDICOM)

        self.ui.auto_seqs_button.clicked.connect(self.automaticSeqLabelling)

        self.ui.apply_seqs_button.clicked.connect(self.saveSequencesFromTable)

        self.ui.apply_new_name_button.clicked.connect(self.changeName)
        self.ui.duplicate_button.clicked.connect(self.duplicateSubject)

        self.ui.add_row_button.clicked.connect(self.addRowTable)

        self.ui.subject_directory_input.textChanged.connect(self.scanDirectory)


        self.ui.saving_progress_bar.hide()





        # Set scene in MRML widgets. Make sure that in Qt designer the top-level qMRMLWidget's
        # "mrmlSceneChanged(vtkMRMLScene*)" signal in is connected to each MRML widget's.
        # "setMRMLScene(vtkMRMLScene*)" slot.
        uiWidget.setMRMLScene(slicer.mrmlScene)


        # Connections
        # These connections ensure that we update parameter node when scene is closed
        self.addObserver(slicer.mrmlScene, slicer.mrmlScene.StartCloseEvent, self.onSceneStartClose)
        self.addObserver(slicer.mrmlScene, slicer.mrmlScene.EndCloseEvent, self.onSceneEndClose)


        """ # Make sure parameter node is initialized (needed for module reload)
        self.initializeParameterNode() """


        # Search for former inputs
        self.loadHistory()


    def cleanup(self) -> None: # Called when the application closes and the module widget is destroyed.
        self.removeObservers()
        if(self.logic.trDirWatcher.directories()):
            self.logic.trDirWatcher.removePaths(self.logic.trDirWatcher.directories())


    def enter(self) -> None: # Called each time the user opeonBrowsePatientDirectoryns this module.
        # Make sure parameter node exists and observed
        """ self.initializeParameterNode()
 """
    def exit(self) -> None: # Called each time the user opens a different module.
        # Do not react to parameter node changes (GUI will be updated when the user enters into the module)
        if self._parameterNode:
            self._parameterNode.disconnectGui(self._parameterNodeGuiTag)
            self._parameterNodeGuiTag = None
            #self.removeObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent, self._checkCanApply)

    def onSceneStartClose(self, caller, event) -> None: # Called just before the scene is closed.
        # Parameter node will be reset, do not use it anymore
        """ self.setParameterNode(None) """

    def onSceneEndClose(self, caller, event) -> None: # Called just after the scene is closed.
        # If this module is shown while the scene is closed then recreate a new parameter node immediately
        """ if self.parent.isEntered:
            self.initializeParameterNode() """

    """ def initializeParameterNode(self) -> None: # Ensure parameter node exists and observed.
        # Parameter node stores all user choices in parameter values, node selections, etc.
        # so that when the scene is saved and reloaded, these settings are restored.

        self.setParameterNode(self.logic.getParameterNode())

        # Select default input nodes if nothing is selected yet to save a few clicks for the user
        if not self._parameterNode.inputVolume:
            firstVolumeNode = slicer.mrmlScene.GetFirstNodeByClass("vtkMRMLScalarVolumeNode")
            if firstVolumeNode:
                self._parameterNode.inputVolume = firstVolumeNode """

    """ def setParameterNode(self, inputParameterNode: Optional[DataManagementParameterNode]) -> None: # Set and observe parameter node.
        # Observation is needed because when the parameter node is changed then the GUI must be updated immediately.
       

        if self._parameterNode:
            self._parameterNode.disconnectGui(self._parameterNodeGuiTag)
            self.removeObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent, self._checkCanApply)
        self._parameterNode = inputParameterNode
        if self._parameterNode:
            # Note: in the .ui file, a Qt dynamic property called "SlicerParameterName" is set on each
            # ui element that needs connection.
            self._parameterNodeGuiTag = self._parameterNode.connectGui(self.ui)
            self.addObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent, self._checkCanApply)
            self._checkCanApply() """

    def _checkCanApply(self, caller=None, event=None) -> None:
        """ if self._parameterNode and self._parameterNode.inputVolume and self._parameterNode.thresholdedVolume:
            self.ui.applyButton.toolTip = _("Compute output volume")
            self.ui.applyButton.enabled = True
        else:
            self.ui.applyButton.toolTip = _("Select input and output volume nodes")
            self.ui.applyButton.enabled = False """

    def onApplyButton(self) -> None:
        """ # Run processing when user clicks "Apply" button.
        with slicer.util.tryWithErrorDisplay(_("Failed to compute results."), waitCursor=True):
            # Compute output
            self.logic.process(self.ui.inputSelector.currentNode(), self.ui.outputSelector.currentNode(),
                               self.ui.imageThresholdSliderWidget.value, self.ui.invertOutputCheckBox.checked)

            # Compute inverted output (if needed)
            if self.ui.invertedOutputSelector.currentNode():
                # If additional output volume is selected then result with inverted threshold is written there
                self.logic.process(self.ui.inputSelector.currentNode(), self.ui.invertedOutputSelector.currentNode(),
                                   self.ui.imageThresholdSliderWidget.value, not self.ui.invertOutputCheckBox.checked, showResult=False) """

    # Module UI controls
    def saveHistory(self, patientDir, patientName = "", igDk = False, igDx = False, igAs = False): # Save input patient to history
        if not os.path.exists(self.historyDir):
            os.mkdir(self.historyDir)
        with open(self.historyPath, 'w') as f:
            f.write(patientDir + "\n" + patientName + "\n" + str(igDk) + "\n" + str(igDx) + "\n" + str(igAs))



    def loadHistory(self): # Get Back former patient
        if os.path.exists(self.historyPath):
            with open(self.historyPath, 'r') as f:
                self.ui.subject_directory_input.setText(f.readline().rstrip("\n"))
                self.ui.subject_combo.currentText = f.readline().rstrip("\n")
                self.ui.ignore_desikan_check.checked = f.readline().rstrip("\n") == "True"
                self.ui.ignore_destrieux_check.checked = f.readline().rstrip("\n") == "True"
                self.ui.ignore_aseg_check.checked = f.readline().rstrip("\n") == "True"

    

    def requestWorkingDirectory(self): # Ask user for patients directory
        wd = self.ui.subject_directory_input.text
        dirName = qt.QFileDialog.getExistingDirectory(None, 'Choose subjects directory', wd)
        if dirName:
            self.ui.subject_directory_input.setText(dirName)
            self.saveHistory(dirName)


    def chooseSubject(self): # Choose subject and init the rest of the module
        wd = self.ui.subject_directory_input.text
        if(wd):
            nom = self.ui.subject_combo.currentText
            if(nom == self.newSubjectText):
                nom = qt.QInputDialog().getText(None, "Input subject name", "Enter subject identifier. EG : pat_AbCd or sub_MiCa")

            if(nom and not nom == "None"):
                ignores = {
                    "Desikan": self.ui.ignore_desikan_check.isChecked(),
                    "Destrieux": self.ui.ignore_destrieux_check.isChecked(),
                    "Aseg": self.ui.ignore_aseg_check.isChecked()
                }
                self.saveHistory(wd, nom, *ignores.values())
                if (self.logic.initPatient(wd, nom, ignores)):
                    for box in self.groupBoxes:
                        box.setEnabled(True)
                        box.setChecked(True)
                    self.ui.working_sub_folder_label.text = "Current subject path : \n"+ self.logic.subject.path
                    self.ui.name_change_section_button.checked = True
                    self.ui.open_section_button.checked = False
                    self.ui.read_bids_area.text = self.logic.subject.workingDir
                    self.ui.bids_dest_area.text = self.logic.subject.workingDir

            else:
                self.warning('Subject Name should follow the naming convention, ie Martin Gregorio should be MaGr')

        else:
            self.warning('Please define the working directory')

    def importDICOM(self): # Browse and import DICOM into the scene
        # Files are imported into a temporary DICOM database, so the current Slicer DICOM database is not impacted.

        # TODO: warn user if data already loaded

        formerFile = self.ui.import_folder_label.text
        if(not os.path.exists(formerFile)):
            formerFile = self.logic.subject.workingDir

        dirName = qt.QFileDialog.getExistingDirectory(None, 'Choose DICOM import directory', formerFile)
        if dirName:
            self.ui.import_folder_label.setText(dirName)
            self.ui.seqs_section_button.enabled = True
            self.ui.seqs_section_button.checked = True

            self.logic.importDICOMSequences(dirName)

            self.ui.seqs_table.setRowCount(max(self.ui.seqs_table.rowCount, len(self.logic.DICOMNodeIDs)))

            self.automaticSeqLabelling()

    def automaticSeqLabelling(self): # Display sequences into the seqs_table
        keyedNames = self.logic.identifyNodeModalititesFromDICOMNames()

        self.setTableColumn(list(keyedNames.keys()), 0)
        self.setTableColumn(list(keyedNames.values()), 1)

    def getTableColumn(self, column = 0): # Extracts all sequence types from the seqs_table widget
        modalities = []
        for i in range(self.ui.seqs_table.rowCount):
            item = self.ui.seqs_table.item(i, column)
            value = item.text() if item else ""
            modalities.append(value)

        return modalities


    def setTableColumn(self, fileNames=[], column = 0): # display files names for each modalities in seqs_table
        for i in range(self.ui.seqs_table.rowCount):
            text = fileNames[i] if len(fileNames) > i else ""
            self.ui.seqs_table.setItem(i, column, qt.QTableWidgetItem(text))

    def addRowTable(self):
        self.ui.seqs_table.setRowCount(self.ui.seqs_table.rowCount + 1)

    def saveSequencesFromTable(self):
        # Save sequences that have been 
        self.ui.saving_progress_bar.show()
        self.ui.saving_progress_bar.setProperty("value", 1)
        progressbar = slicer.util.createProgressDialog()
        progressbar.value = 0
        progressbar.setLabelText("Saving sequences as nifti")

        modalities = self.getTableColumn(0)
        files = self.getTableColumn(1)
        DICOMImportedSceneNodes = self.logic.getSequencesImportedFromDICOM()

        nodesToSaveAndTypes = []

        progressbar.value = 1
        slicer.app.processEvents()
        for node in DICOMImportedSceneNodes:
            nodeName = node.GetName()
            if(nodeName in files):
                chosenModality = modalities[files.index(nodeName)]
                if(chosenModality):
                    nodesToSaveAndTypes.append((node, chosenModality))

        self.setTableColumn([], 1) # Reset the seqs_ree

        self.logic.applyNamingSchemeToScene(nodesToSaveAndTypes)


        # Save Nodes with progressbar
        for progress in self.logic.saveNodesNiftiGenerator(nodesToSaveAndTypes):
            self.ui.saving_progress_bar.setProperty("value", progress)
            progressbar.value = progress
            slicer.app.processEvents()
        self.ui.saving_progress_bar.hide()

        self.ui.seqs_section_button.setChecked(False)
        self.ui.seqs_section_button.setEnabled(False)

        self.message("Importation terminée")



    def message(self, message, title = None):
        print("Message Data Management Module")
        if title :
            print(title)
            
        print(message)
        slicer.util.delayDisplay(message, 3000)
   
    def warning(self, message, title=None):
        print("WARNING")
        if title :
            print(title)
            
        print(str(message))
        slicer.util.errorDisplay("Data Management Module : "+ str(message), 3000)


    def changeName(self):
        nameInput = self.ui.change_subject_name_input.text
        try:
            self.logic.setPatientName(nameInput) # fails if invalid input
            self.updateNameUI()

        except Warning as war:
            self.warning(war)

        
    def duplicateSubject(self):
        nameInput = self.ui.change_subject_name_input.text
        try:
            self.logic.duplicateSubject(nameInput) # fails if invalid input
            self.updateNameUI()

        except Warning as war:
            self.warning(war)
    
    def updateNameUI(self):
        self.ui.working_sub_folder_label.text = "Current subject path : \n" + self.logic.subject.path

        self.ui.subject_directory_input.text = self.logic.subject.workingDir
        self.scanDirectory()
        self.ui.subject_combo.currentText = self.logic.subject.folderName

        self.ui.change_subject_name_input.setText("")

        self.ui.seqs_section_button.setChecked(False)
        self.ui.seqs_section_button.setEnabled(False)

        self.saveHistory(self.logic.subject.workingDir, self.logic.subject.folderName, *self.logic.ignores.values())


    def scanDirectory(self):
        directory = self.ui.subject_directory_input.text
        if(os.path.isdir(directory) and os.path.exists(directory)):
            subjects = self.logic.getDirSubjects(directory)
            self.setSubjCombo(subjects)
        else:
            self.setSubjCombo([])

    def setSubjCombo(self, subList):
        while(self.ui.subject_combo.count):
            self.ui.subject_combo.removeItem(0)
        for sub in subList:
            self.ui.subject_combo.addItem(sub)

        if(len(subList)):
            self.ui.subject_combo.addItem(self.newSubjectText)
        else:
            self.ui.subject_combo.addItem("None")

        self.ui.open_button.enabled = len(subList) > 0
        self.ui.subject_combo.enabled = len(subList) > 0

























#           
# DataManagementLogic
#

def isNifti(fileName):
    splitPath = os.path.splitext(fileName)
    return splitPath[1] == ".gz" and os.path.splitext(splitPath[0])[1] == ".nii"

def isTransform(fileName):
    return os.path.splitext(fileName)[1] == ".tfm"

class SubjectData(PatientModelEpilepsy):
    workingDir = None
    folderName = None
    niftiDir = None
    path = None
    #regMatDir = None
    segsDir = None
    mriSegsDir = None
    segPaths = None
    brainvisaModelsPaths = None
    meshRefDir=None
    acpcPath = None
    subjectHierarchyFolderID = None


    model = None

    defaultModality = "t1mri"
    brainvisaModelsAttributes = {'Left Pial': {'SetVisibility2D': True,'SetColor': (1.0, 0.7, 0.7),'SetOpacity': 1.0,'SetBackfaceCulling': False},
        'Right Pial': {'SetVisibility2D': True,'SetColor': (1.0, 0.7, 0.7),'SetOpacity': 1.0,'SetBackfaceCulling': False},
        'Left Gray/White': {'SetVisibility2D': True,'SetColor': (1.0, 0.7, 0.7),'SetOpacity': 1.0,'SetBackfaceCulling': False},
        'Right Gray/White': {'SetVisibility2D': True,'SetColor': (1.0, 0.7, 0.7),'SetOpacity': 1.0,'SetBackfaceCulling': False},
        'Left Sulci': {'SetVisibility2D': False,'SetColor': (1.0,1.0, 1.0),'SetOpacity': 1.0,'SetBackfaceCulling': False},
        'Right Sulci': {'SetVisibility2D': False,'SetColor': (1, 1.0, 1.0),'SetOpacity': 1.0,'SetBackfaceCulling': False},
        'Head':{'SetVisibility2D': True,'SetColor': (1, 0.75, 0.8),'SetOpacity': .05,'SetBackfaceCulling': False}}


    niftiNodeIds = {}

    transformsData = []

    segmentationNodeIds = {}



    def __init__(self, folderName, workingDir):
        super().__init__(folderName, rootDir=workingDir)

        self.workingDir = workingDir
        self.folderName = folderName
        self.path = os.path.join(self.workingDir, self.folderName)
        self.niftiDir = os.path.join(self.path, "nifti")
        #self.regMatDir = os.path.join(self.path, "reg_mat")
        self.segsDir = os.path.join(self.path, "segmentations")
        self.mriSegsDir = os.path.join(self.segsDir, f"ref_{self.defaultModality}")

        self.acpcPath = os.path.join(self.niftiDir, f'{self.folderName}_acpc.ros')

        self.meshRefDir = os.path.join(self.path, 'mesh')
        self.brainvisaModelsFileNames = {'Left Pial': f'{self.folderName}_cortex_gray_pial_left.vtk',
                                    'Right Pial': f'{self.folderName}_cortex_gray_pial_right.vtk',
                                    'Left Gray/White': f'{self.folderName}_cortex_gray_white_left.vtk',
                                    'Right Gray/White': f'{self.folderName}_cortex_gray_white_right.vtk',
                                    'Left Sulci': f'{self.folderName}_sulci_left.vtk',
                                    'Right Sulci': f'{self.folderName}_sulci_right.vtk',
                                    'Head': f'{self.folderName}_head.vtk'}
        
        shn = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        sceneItemID = shn.GetSceneItemID()
        self.subjectHierarchyFolderID = shn.CreateFolderItem(sceneItemID, self.folderName)










class DataManagementLogic(ScriptedLoadableModuleLogic):
    """Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """
    subject = None

    sequence_identifiers = {
        "t1mri" : "MPRAGE T1 AX STRICT GADO", 
        "t1mri_pre" : "T1mpr_SAG_Tal", 
        "ct_pre" : "CRANE SANS IV", 
        "ct_pre2" : "CRANE OSSEUX", 
        "flair" : "FLAIR", 
        "spect" : "BRAIN MAC ZTE"
    }

    DICOMNodeIDs = []

    ignores = {
        "Desikan": False,
        "Aseg": False,
        "Destrieux": False,
    }

    trDirWatcher = qt.QFileSystemWatcher()



    def __init__(self) -> None: #Can be used for initializing member variables.
        ScriptedLoadableModuleLogic.__init__(self)

    def initPatient(self, directory, patFolderName, ignores = None): # Import / create a patient folder at directery location

        if(not os.path.isdir(directory)):
            raise ValueError('Data Management Module : Provided working directory does not exist')

        if(ignores):
            self.ignores = ignores


        if(self.subject):
            if(not slicer.util.confirmOkCancelDisplay("Un patient est déjà chargé, toute modification non sauvegardée sera perdue.")):
                return False
            
            """ folderOldSubjectItemID = shn.GetItemByName(self.subject.folderName) # previous subject folder name
            if(folderOldSubjectItemID):
                shn.RemoveItem(folderOldSubjectItemID) """
        slicer.mrmlScene.Clear()
        if(self.trDirWatcher.directories()):
            self.trDirWatcher.removePaths(self.trDirWatcher.directories())



        
        self.modalities = []


        self.subject = SubjectData(patFolderName, directory)


        if(not os.path.isdir(self.subject.path)):
            os.mkdir(self.subject.path)
        if(not os.path.isdir(self.subject.niftiDir)):
            os.mkdir(self.subject.niftiDir)
        if(not os.path.isdir(self.subject.maskDir)):
            os.mkdir(self.subject.maskDir)
        if(not os.path.isdir(self.subject.regMatDir)):
            os.mkdir(self.subject.regMatDir)
        if(not os.path.isdir(self.subject.segsDir)):
            os.mkdir(self.subject.segsDir)
        if(not os.path.isdir(self.subject.mriSegsDir)):
            os.mkdir(self.subject.mriSegsDir)


       



        # Nifti folder
        modalities = []

        niftifiles = os.listdir(self.subject.niftiDir)
        for fileName in niftifiles:
            if(isNifti(fileName)):

                filePath = os.path.join(self.subject.niftiDir, fileName)
                node = slicer.util.loadVolume(filePath)
                # node.SetName(re.sub(r'_\d+$', '', node.GetName())) # remove numbers that are added at the end of the nodename
                
                nodeSource = self.getNodeSourceFromName(fileName)
                node.SetName(nodeSource)
                self.moveNodeInSubjectHierarchy(node, ["nifti"])
                modalities.append(nodeSource)
                self.subject.niftiNodeIds[nodeSource] = node.GetID()






        # Transforms
        self.updateTransformsFolder()
        # Setup folder monitoring
        self.setupWatcherDirectories()

        with os.scandir(self.subject.mriSegsDir) as dirContent:
            for fObject in dirContent:
                if(fObject.is_dir()):
                    self.importSegmentationsFromFolder(fObject.path, fObject.name)
        
        # TODO : Import mask and mesh here and only apply transforms in the for

        for modality in modalities:
            transformNodeData = self.getTransformFromScene(modFrom = modality, modTo = "acpc")


            if(transformNodeData):

                # mask
                masksPathModality = os.path.join(self.subject.maskDir, f"ref_{modality}")
                if(os.path.exists(masksPathModality)):
                    with os.scandir(masksPathModality) as files:
                        for masksFile in files:
                            if(masksFile.is_file() and isNifti(masksFile.name)):
                                node = slicer.util.loadVolume(masksFile.path)
                                cleanName = re.sub(f'^{self.subject.folderName}_', '', re.sub(r'_\d+$', '', node.GetName())) # remove numbers that are added at the end of the nodename + remove subject name
                                node.SetName(cleanName) 
                                node.SetAndObserveTransformNodeID(transformNodeData["ID"])
                                self.moveNodeInSubjectHierarchy(node, ["mask", f"ref_{modality}"])


                if modality == self.subject.defaultModality:
                    for idList in self.subject.segmentationNodeIds.values():
                        for nodeID in idList:
                            segNode = slicer.mrmlScene.GetNodeByID(nodeID)
                            if(segNode):
                                segNode.SetAndObserveTransformNodeID(transformNodeData["ID"])
                            else:
                                print("Cannot find segmentation node with id :", nodeID)


                # Load Brainvisa models
                modelsDir = os.path.join(self.subject.meshRefDir, f"ref_{modality}")
                if(os.path.exists(modelsDir)):
                    for modelType in self.subject.brainvisaModelsFileNames:

                        modelFileName = self.subject.brainvisaModelsFileNames[modelType]
                        modelPath = os.path.join(modelsDir, modelFileName)

                        model = self.loadModel(modelPath, self.subject.brainvisaModelsAttributes[modelType])
                        if(model):

                            cleanName = re.sub(f'^{self.subject.folderName}_', '', re.sub(r'_\d+$', '', model.GetName())) # remove numbers that are added at the end of the nodename + remove subject name
                            model.SetName(cleanName)
                           
                            model.SetAndObserveTransformNodeID(transformNodeData["ID"])
                            self.moveNodeInSubjectHierarchy(model, ["mesh", f"ref_{modality}"])

                    


        return True
    

    def setupWatcherDirectories(self):
        self.trDirWatcher.addPath(self.subject.regMatDir)
        self.trDirWatcher.directoryChanged.connect(self.pullDirectory)

    def pullDirectory(self, dirPath):
        if(dirPath == self.subject.regMatDir):
            self.updateTransformsFolder(False)

    def updateTransformsFolder(self, askOverride = True):
        # Remove nodes that were delete from the scene
        self.subject.transformsData = [tr for tr in self.subject.transformsData if slicer.mrmlScene.GetNodeByID(tr["ID"])]

        # Add new nodes from folder
        self.importTransformsFromFolder(self.subject.regMatDir, ["reg_mat"], askOverride)

    def importTransformsFromFolder(self, location, subjectHierarchy, askOverride = True, applyTransformsTo = "acpc"):
        with os.scandir(location) as dirContent:
            for fObject in dirContent:
                if(fObject.is_dir()):
                    self.importTransformsFromFolder(fObject.path,[*subjectHierarchy, fObject.name], askOverride) # recursive

                if(fObject.is_file() and isTransform(fObject.name)):
                    nodeSourceModality, nodeDestinationModality = self.getTransformModalitiesFromName(fObject.name)
                    if(nodeSourceModality and nodeDestinationModality):
                        similarTransform = self.getTransformFromScene(modFrom=nodeSourceModality, modTo=nodeDestinationModality)
                        
                        if(nodeSourceModality in self.subject.niftiNodeIds):
                            niftiNode = slicer.mrmlScene.GetNodeByID(self.subject.niftiNodeIds[nodeSourceModality])
                            if(niftiNode):
                                if(similarTransform is not None):
                                    if(not askOverride):
                                        self.moveNodeInSubjectHierarchy(slicer.mrmlScene.GetNodeByID(similarTransform["ID"]), [*subjectHierarchy])
                                        continue
                                    if(not slicer.util.confirmYesNoDisplay(f"Voulez-vous remplacer la transformation : {similarTransform['name']}")):
                                        continue # this will skip the rest of the code for this loop
                                    else:
                                        slicer.mrmlScene.RemoveNode(slicer.mrmlScene.GetNodeByID(similarTransform["ID"]))
                                        self.subject.transformsData.remove(similarTransform)
                                
                                ac2pc=AffineMatrix(np.array([[1.0, 0, 0, 0],[0, 1.0, 0, 0],[0, 0, 1.0, 0],[0, 0, 0, 1.0]]))
                                transfMatrix = AffineMatrix(fObject.path)
                                transfMatrix.leftMultiply(ac2pc)
                                transformNode = self.getTransformNodeFromAffine(transfMatrix,f"{nodeSourceModality}2{nodeDestinationModality}")
                                self.moveNodeInSubjectHierarchy(transformNode, [*subjectHierarchy])
                                
                                if(nodeDestinationModality == applyTransformsTo):
                                    niftiNode.SetAndObserveTransformNodeID(transformNode.GetID())

                                self.subject.transformsData.append({
                                    "name": transformNode.GetName(),
                                    "ID": transformNode.GetID(),
                                    "modFrom": nodeSourceModality,
                                    "modTo": nodeDestinationModality
                                })
            
    def importSegmentationsFromFolder(self, directory, subfolder):
        with os.scandir(directory) as dirContent:
            for fObject in dirContent:
                if(fObject.is_file() and self.isSegmentationFile(fObject.name)):
                    segType = self.getSegmentationType(fObject.name)
                    if not self.ignores[segType]:
                        segNode = slicer.util.loadSegmentation(fObject.path)
                        self.moveNodeInSubjectHierarchy(segNode, ["segmentations", subfolder])
                        cleanName = re.sub(r'_\d+$', '', segNode.GetName())
                        segNode.SetName(cleanName)


                        if(not subfolder in self.subject.segmentationNodeIds.keys()):
                            self.subject.segmentationNodeIds[subfolder] = []
                        self.subject.segmentationNodeIds[subfolder].append(segNode.GetID())

    def registerSegmentationNodes(self, segNodes, subfolder):
        transformNodeData = self.getTransformFromScene(modFrom = self.subject.defaultModality, modTo = "acpc")

        if(not subfolder in self.subject.segmentationNodeIds.keys()):
            self.subject.segmentationNodeIds[subfolder] = []
        
        for node in segNodes:
            nodeID = node.GetID()
            if(not nodeID in self.subject.segmentationNodeIds[subfolder]):
                self.subject.segmentationNodeIds[subfolder].append(nodeID)
                segNode = slicer.mrmlScene.GetNodeByID(nodeID)
                if(transformNodeData and "ID" in transformNodeData):
                    segNode.SetAndObserveTransformNodeID(transformNodeData["ID"])
            self.moveNodeInSubjectHierarchy(node, ["segmentations", subfolder])

    def getTransformNodeFromAffine(self,affineToCACP,name):
         
        vtk_transform=affineToCACP.getVTK4x4Matrix()
        transformNode = slicer.vtkMRMLLinearTransformNode()
        transformNode.SetName(name)
        slicer.mrmlScene.AddNode(transformNode)
        transformNode.SetMatrixTransformToParent(vtk_transform)
        return transformNode


    def loadModel(self, modelPath, attributes = {}):
        if(os.path.exists(modelPath)):

            modelNode = slicer.modules.models.logic().AddModel(modelPath, slicer.vtkMRMLStorageNode.CoordinateSystemRAS)
            modelNode.SetName(re.sub(r'_\d+$', '', modelNode.GetName())) # remove numbers that are added at the end of the nodename

            displayNode = modelNode.GetDisplayNode()
            for key, value in list(attributes.items()):
                getattr(displayNode, key)(value)
            return modelNode
        
        print(f"The model {modelPath} could not be found.")

    """ def extractElementFromList(self, list, eval):
        for x in list:
            if eval(x):
                return x
        return None """


    def importDICOMSequences(self, directory): # Derivates from epiSTIM/importDICOMSpreOp.py
        loadedNodeIDs = []

        with DICOMUtils.TemporaryDICOMDatabase() as db: 
            DICOMUtils.importDicom(directory, db)
            patientUIDs = db.patients() 
            for patientUID in patientUIDs: 
                loadedNodeIDs.extend(DICOMUtils.loadPatientByUID(patientUID)) 

        self.DICOMNodeIDs = loadedNodeIDs
    
    def getSequencesImportedFromDICOM(self):
        nodesResult = []
        for nodeID in self.DICOMNodeIDs:
            nodesResult.append(slicer.mrmlScene.GetNodeByID(nodeID))

        return(nodesResult)




    def getNodesFromScene(self):
        return(slicer.mrmlScene.GetNodesByClass("vtkMRMLScalarVolumeNode"))

    """ def getTransformsFromScene(self):
        return(slicer.mrmlScene.GetNodesByClass("vtkMRMLLinearTransformNode")) """
        

    def identifyNodeModalititesFromDICOMNames(self):
        nodes = self.getSequencesImportedFromDICOM()
        keyedNames = {}
        for modality in self.sequence_identifiers:
            modalityIdentifyer = self.sequence_identifiers[modality]
            for node in nodes:
                nodeName = node.GetName()
                if(modalityIdentifyer in nodeName):
                    keyedNames[modality] = node.GetName()
                    break
        
        return keyedNames


    def saveNodesNiftiGenerator(self, nodesAndTypes):
        nodes = []
        i=1
        length = len(nodesAndTypes)
        for (node,nodeType) in nodesAndTypes:
            node.SetAttribute("modality", nodeType)

            self.subject.niftiNodeIds[nodeType] = node.GetID()
            nodeName = self.getNodeNameFromType(nodeType)
            nodes.append(node)
            self.moveNodeInSubjectHierarchy(node, ["nifti"])

            # Apply transform if available
            for transformdata in self.subject.transformsData:
                if(transformdata["from"] == nodeType and transformdata["to"] == "acpc"):
                    node.SetAndObserveTransformNodeID(transformdata["ID"])
                    break

            # Save nifti file
            slicer.util.saveNode(node,os.path.join(self.subject.niftiDir,f'{nodeName}.nii.gz'))
            yield(i/(length)*100)
            i+=1

        self.DICOMNodeIDs = []

    def saveNodesNifti(self, nodesAndTypes):
        for val in self.saveNodesNiftiGenerator(nodesAndTypes): # This line does all the work (generator)
            pass

    def applyNamingSchemeToScene(self, nodesAndTypes):
        for (node,nodeType) in nodesAndTypes:
            node.SetName(self.getNodeNameFromType(nodeType))



    def getNodeNameFromType(self, nodeType):
        return f'{self.subject.folderName}_{nodeType}'



    """ def importACPCFile(self, filePath):
        if(filePath and os.path.exists(filePath) and os.path.splitext(filePath)[1] == ".ros"):
            shutil.copy(filePath, self.subject.acpcPath)
            return True """

    def setPatientName(self, newName):
        if(self.trDirWatcher.directories()):
            self.trDirWatcher.removePaths(self.trDirWatcher.directories())
        if(self.subject is not None):
            if(newName and len(newName) > 4):

                newSubject = SubjectData(newName, self.subject.workingDir)
                if(not os.path.exists(newSubject.path)):

                    # Rename all files
                    for root, dirs, files in os.walk(self.subject.path):
                        for file in files:
                            filePath = os.path.join(root, file)
                            destinationPath = os.path.join(root, re.sub(self.subject.folderName, newSubject.folderName, file))
                            os.rename(filePath, destinationPath)

                    # rename the main subject directory
                    shutil.move(self.subject.path, newSubject.path)

                    self.changeNodesSubjectName(newSubject.folderName)

                    self.subject = newSubject
                    self.setupWatcherDirectories()

                else:
                    raise Warning("Data Management Module : A file with the same name already exists in the working folder")
            else:
                raise Warning('Invalid subject name')
        else:
            raise Warning('Open a subject before renaming it')



    def duplicateSubject(self, newName):
        if(self.trDirWatcher.directories()):
            self.trDirWatcher.removePaths(self.trDirWatcher.directories())

        if(self.subject is not None):
            if(newName and len(newName) > 4):

                newSubject = SubjectData(newName, self.subject.workingDir)
                if(not os.path.exists(newSubject.path)):

                    def customCopy(src, dst, *args, follow_symlinks=True):
                        splitDest = os.path.split(dst)
                        newDest = os.path.join(splitDest[0], re.sub(self.subject.folderName, newSubject.folderName, splitDest[1]))
                        return shutil.copy2(src, newDest, follow_symlinks=True)

                    shutil.copytree(self.subject.path, newSubject.path, copy_function=customCopy)
                    
                    self.changeNodesSubjectName(newSubject.folderName)

                    self.subject = newSubject
                    self.setupWatcherDirectories()

                else:
                    raise Warning("Data Management Module : A file with the same name already exists in the working folder")
            else:
                raise Warning('Invalid subject name')
        else:
            raise Warning('Open a subject before renaming it')

    def changeNodesSubjectName(self, newName):
        # Rename The folder in Subject Hierarchy
        shn = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        folderSubjectItemID = self.subject.subjectHierarchyFolderID
        if(folderSubjectItemID):
            shn.SetItemName(folderSubjectItemID, f'{newName}')


        # Rename nodes of the folder in Subject Hierarchy
        childrenNodesIDs = vtk.vtkIdList()

        shn.GetItemChildren(folderSubjectItemID, childrenNodesIDs, True)
        for i in range(childrenNodesIDs.GetNumberOfIds()):
            childNodeID = childrenNodesIDs.GetId(i)
            nodeName = shn.GetItemName(childNodeID)
            shn.SetItemName(childNodeID, re.sub(self.subject.folderName, newName, nodeName))


    def moveNodeInSubjectHierarchy(self, node, folderPathAsList=[]):
        shn = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        sceneItemID = shn.GetSceneItemID()
        nodeID = shn.GetItemByDataNode(node)
        if nodeID:


            # Top - Down : get to the destination folder, creating the necessary folders
            pointedFolderItemID = self.subject.subjectHierarchyFolderID


            for folderName in folderPathAsList:
                # Pick folder or create it if it doesn't exist
                nextPointedFolderItemID = shn.GetItemChildWithName(pointedFolderItemID,folderName)
                if(not nextPointedFolderItemID):
                    nextPointedFolderItemID = shn.CreateFolderItem(pointedFolderItemID, folderName)
                
                pointedFolderItemID = nextPointedFolderItemID


            

            # Flag nodes in the destination if same name with old prefix
            sameNameNodeID = shn.GetItemChildWithName(pointedFolderItemID, node.GetName())
            if(sameNameNodeID and not sameNameNodeID == nodeID):
                shn.SetItemName(sameNameNodeID, f'old_{node.GetName()}')
        
 
            shn.SetItemParent(nodeID,pointedFolderItemID)




    """ def bundleNodesInPatientHierarchyFolder(self, nodes, subfolders):
        for node in nodes:
            self.moveNodeInSubjectHierarchy(node, subfolders)

    def dictionnaryNodesBundler(self, dico, subfolder = None): # bundles nodes from dico into subject hierarchy
        #dico cannot contain other dicos. TODO : add this functionnality
        for key in dico:
            location = [subfolder, key] if subfolder else [key]
            self.bundleNodesInPatientHierarchyFolder(dico[key], location) """

    def isSegmentationFile(self, fileName):
        return os.path.splitext(fileName)[1] == ".vtm"
       
    def getSegmentationType(self, fileName):
        for segType in self.ignores.keys():
            if(segType in fileName):
                return segType

        return None
    
    def getNodeSourceFromName(self, fileName):

        busca = self.subject.folderName +"_(.*)\.nii"
        compi=re.compile(busca)
        r=compi.search(fileName)
        if(r):
            return r.group(1)    

    def getTransformModalitiesFromName(self, transformFileName):
        busca = "_slicer_(.*)_2_(.*)\."
        compi=re.compile(busca)
        r=compi.search(transformFileName)
        if(r):
            return r.group(1), r.group(2)
        else:
            return (None, None)



    """ def showInACPC(self):
        ac2pc=np.array([[1.0, 0, 0, 0],[0, 1.0, 0, 0],[0, 0, 1.0, 0],[0, 0, 0, 1.0]])
        if os.path.exists(self.subject.acpcPath) is not None :
            ca,cp,ih=self.readAPCFileOr(acpFilePath)  
            ac  = np.array(ca,dtype=float)
            pc = np.array(cp,dtype=float)

            ac2pc[1,3] = np.linalg.norm(ac - pc)
            self.ac2pc = AffineMatrix(ac2pc) """

    def getDirSubjects(self, directory):
        result = []
        with os.scandir(directory) as content:
            for folder in content:
                if(folder.is_dir):
                    result.append(folder.name)
        return result
    
    def registerTransformNodeFromMatrix(self, transformMatrix, sourceModality, destinationModality):
        nodeName = f"{sourceModality}2{destinationModality}"
        filename = f"{self.subject.folderName}_slicer_{nodeName}.tfm" # 
        savePath = os.path.join(self.subject.regMatDir, filename)
        aux = savePath.replace("tfm","xfm").replace("slicer","w2w")

        transformNode = self.getTransformNodeFromAffine(transformMatrix, nodeName)

        transformMatrix.writeToXFMFile(aux)
        transformMatrix.writeToSlicerFile(savePath)
            
        self.subject.transformsData.append({
            "name": transformNode.GetName(),
            "ID": transformNode.GetID(),
            "modFrom": sourceModality,
            "modTo": destinationModality
        })

        self.moveNodeInSubjectHierarchy(transformNode, ["reg_mat"])

        if(sourceModality in self.subject.niftiNodeIds):
            niftiNode = slicer.mrmlScene.GetNodeByID(self.subject.niftiNodeIds[sourceModality])
            niftiNode.SetAndObserveTransformNodeID(transformNode.GetID())

    def registerTransformNode(self, transformNode, sourceModality, destinationModality):
        nodeName = f"{sourceModality}2{destinationModality}"
        filename = f"{self.subject.folderName}_slicer_{nodeName}.tfm"
        savePath = os.path.join(self.subject.regMatDir, filename)
        aux = savePath.replace("tfm","xfm").replace("slicer","w2w")

        self.subject.transformsData.append({
            "name": transformNode.GetName(),
            "ID": transformNode.GetID(),
            "modFrom": sourceModality,
            "modTo": destinationModality
        })

        self.moveNodeInSubjectHierarchy(transformNode, ["reg_mat"])

        if(sourceModality in self.subject.niftiNodeIds): # TODO : Keep this or create an applyTransformsToNodes general function to apply it to masks, segmentations, etc.. if necessary
            niftiNode = slicer.mrmlScene.GetNodeByID(self.subject.niftiNodeIds[sourceModality])
            niftiNode.SetAndObserveTransformNodeID(transformNode.GetID())
        
        transformNode.SetName(nodeName)
        slicer.util.saveNode(transformNode,savePath)

    def registerNode(self, node, subjectLocation, saveName = None):
        if(saveName is None):
            saveName = node.GetName()
        self.moveNodeInSubjectHierarchy(node, subjectLocation)
        savePath = os.path.join(self.subject.path, *subjectLocation, saveName)
        if(not os.path.exists(os.path.dirname(savePath))):
            os.makedirs(os.path.dirname(savePath))
        slicer.util.saveNode(node, savePath)

    def getTransformFromScene(self, **kwargs):
        for tr in self.subject.transformsData:
            hasAll = True
            for argName in kwargs:
                if not (argName in tr and tr[argName] == kwargs[argName]):
                    hasAll = False
            if(hasAll):
                return tr































#
# DataManagementTest
#



class DataManagementTest(ScriptedLoadableModuleTest):
    """
    This is the test case for your scripted module.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        """Do whatever is needed to reset the state - typically a scene clear will be enough."""
        slicer.mrmlScene.Clear()

    def runTest(self):
        """Run as few or as many tests as needed here."""
        self.setUp()
        self.test_DataManagement1()

    def test_DataManagement1(self):
        """Ideally you should have several levels of tests.  At the lowest level
        tests should exercise the functionality of the logic with different inputs
        (both valid and invalid).  At higher levels your tests should emulate the
        way the user would interact with your code and confirm that it still works
        the way you intended.
        One of the most important features of the tests is that it should alert other
        developers when their changes will have an impact on the behavior of your
        module.  For example, if a developer removes a feature that you depend on,
        your test should break so they know that the feature is needed.
        """

        self.delayDisplay("Starting the test")

        # Get/create input data
        DICOMPath = "/home/martin.gregorio/Apps/Slicer modules/epiSTIMModules/DataManagement/Testing/data/pat_TeSt_pre"
        testDir = "/home/martin.gregorio/Apps/Slicer modules/epiSTIMModules/DataManagement/Testing/data/"
        patTest = "TeSt"

        

        # Test the module logic
        logic = DataManagementLogic()

        logic.initPatient(testDir, patTest)

        logic.importDICOMSequences(DICOMPath)
        importedSeqs = logic.getSequencesImportedFromDICOM()

        if(not len(importedSeqs)):
            raise ValueError("Epi test : DICOM Sequences failed importation")

        keyedNames = logic.identifyNodeModalititesFromDICOMNames()


        if(not len(keyedNames)):
            raise ValueError("Epi test : Automatic sequence identification failed on all imported sequences")


        nodesAndTypes = []
        for key in keyedNames:
            node = None
            name = keyedNames[key]
            for seq in importedSeqs:
                if(seq.GetName() == name):
                    node = seq
                    break
                    
            nodesAndTypes.append((node, key))
        logic.saveNodesNifti(nodesAndTypes)

        logic.setPatientName("TeMp")
        shn = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        folderSubjectItemID = shn.GetItemByName(f'pat_TeMp')
        if(not folderSubjectItemID):
            raise ValueError("Epi test : Renaming subject to TeMp failed as parent folder could not be found")

        logic.setPatientName("TeSt")


        print("Test Passed 😃")
        self.delayDisplay("Test passed", 8000)
