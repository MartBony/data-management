# Data Management module - DM

This is a slicer module to manage files from EPISTIM.

## General behaviour
The module now automatically keeps track of what is happening in the following folders:
- Transforms (reg_mat)

This means that any changes in these folders will be instantly reflected in slicer.

## Python access to this module
### Access to this module from python
The preferred method for accessing the Data Management module is to use try ... catch. Here is an example
```python
try:
    DMLogic = slicer.modules.DataManagementWidget.logic
    niftiIDs = DMLogic.subject.niftiNodeIds
    doThings(niftiIDs)
except AttributeError as err:
    slicer.util.errorDisplay("Please import sequences using Data Management Module")
    print(err)
```
In this example, if the DM widget wans't opened or initiated, the logic or subject attribute won't exist and an `AttributeError` would be thrown.

### Get subject data
The module stores data about the current subject that can be extracted with the following commands :
```python
subjectsDir = DMLogic.subject.workingDir # The working directory that contains all the subjects
folderName = DMLogic.subject.folderName # The name of the current subject folder (ex : sub_FeAr)
subPath = DMLogic.subject.path # The absolute path of the current subject folder
regMatDir = DMLogic.subject.regMatDir # The directory of all transforms
# etc..., see the source code or the PatientModelEpilepsy class

```

Additionally, the subject class extends from PatientModelEpilepsy (epiSTIM>Core>PatientModelEpilepsy.py) and therefore all the associated data.

### Get scene nodes data
The module keeps track of all nodes that were imported in the scene with it. Another module can access these nodes either through the MRML Scene directly by names, or through the DM module by ID.

You can then access information about the **nifti sequences** :
```python
# Dictionary of (modality : nodeID) of nifti sequences imported through the DM module
nodeIDs = DMLogic.subject.niftiNodeIds
```

Or **transforms** :
```python
# List of transform info about transform nodes imported through the DM module
transformsData = DMLogic.subject.transformsData
```
Each element of this list is a dictionary with the following fields :
| Field | Description |
| ----- | ----------- |
|name|Name of the node|
|ID|ID of the node|
|modFrom|Initial reference frame for the transformation|
|modTo|Final reference frame for the transformation|

You can find a specific element from this list of transform with the function `getTransformFromScene`, for instance:
```python
transformT12ACPC = DMLogic.getTransformFromScene(modFrom = "t1mri", modTo = "acpc")
transformData = DMLogic.getTransformFromScene(ID = myTransformID)
transformData = DMLogic.getTransformFromScene(name = myTransformName)
```

### Register a new node
The DM module allows you to register a new node inside the subject folder.

For **transforms**, you can save a new transform node by providing :
- Either the transformMatrix (instance of the AffineMatrix class)
- Or a transform node already in the scene
as well as the two modalities that define the transform (input modality and output modality). Here are the functions :
```python
DMLogic.registerTransformNodeFromMatrix(t12ACPCMatrix, "t1mri", "acpc")
DMLogic.registerTransformNode(t12ACPCNode, "t1mri", "acpc")
```

### Move nodes
You can ask the DM module to move a node inside of the subject folder in the hierarchy depending on the nature of the node.
```python
DMLogic.moveNodeInSubjectHierarchy(nodeToMove, ["subfolders", "of", "destination"])
# For example for a segmentation node
DMLogic.moveNodeInSubjectHierarchy(segNode, ["segmentations"])
```
This deosn't affect the subject folder in the file system, it just changes the position in the subject hierarchy in the data module.

### Pull changes from the subject folder
The module can update the scene with new data available in the subject folder