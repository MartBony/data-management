from .PathUtils import ensureDir
import numpy as np
import numpy.linalg as la
import os
import vtk

FLIPX = np.array([[-1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
FLIPY = np.array([[1, 0, 0, 0],[0, -1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
FLIPZ = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, -1, 0],[0, 0, 0, 1]])
FLIPXY = np.array([[-1, 0, 0, 0],[0, -1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
FLIPXZ = np.array([[-1, 0, 0, 0],[0, 1, 0, 0],[0, 0, -1, 0],[0, 0, 0, 1]])
FLIPXYZ = np.array([[-1, 0, 0, 0],[0, -1, 0, 0],[0, 0, -1, 0],[0, 0, 0, 1]])
IDENTITY = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])


class AffineMatrix:
  def __init__(self,matrix=None): 

    self.matrix = np.identity(4, float)
    if matrix is not None:
    # intialize to identity
    # read input parameter and initialize matrix accordingly
    
    # another instance of AffineMatrix (deep copy the matrix)
      if isinstance(matrix,AffineMatrix):
        self.matrix = matrix.matrix.copy()
      
    # VTK Matrix
      elif isinstance(matrix,vtk.vtkMatrix4x4):
        self.readFromVTK4x4Matrix(matrix)
        
    # VTK Transform    
      elif isinstance(matrix,vtk.vtkTransform):
        vtkMatrix = matrix.GetMatrix()
        self.readFromVTK4x4Matrix(vtkMatrix)
        
    # MRML linear transform node  
      elif isinstance(matrix,vtk.vtkObject) and matrix.GetClassName() == 'vtkMRMLLinearTransformNode':
        vtkMatrix = vtk.vtkMatrix4x4()
        matrix.GetMatrixTransformToWorld(vtkMatrix)
        self.readFromVTK4x4Matrix(vtkMatrix)
      
    # numpy array of floats 
      elif type(matrix) == np.ndarray:
        if matrix.shape == (4,4):
          self.matrix = matrix.astype(float)
        else:
          raise Exception("Input is numpy array matrix but shape is not 4x4")
      
    # flat list of floats
      elif type(matrix) == list:
        if len(matrix) == 16: 
          npMatrix = np.array(matrix)  
          npMatrix = npMatrix.reshape(4,4)        
          self.matrix = npMatrix.astype(float)
        else:
          raise Exception("Input is a list but length is not 16")
          
    # path to an existing file
      elif type(matrix) == str:
        matrixPath = matrix
        if os.path.exists(matrix):
          self.readFromFile(matrixPath)
        else:
          raise Exception('Input is a string, but is not a path to an existing file')
      
    # others
      else:
        raise Exception('Input matrix type not handled. It must be a list a numpy array or a path to an existing file')
          
  def readFromVTK4x4Matrix(self,vtkMatrix):
    """
    Reads from a VTK4x4Matrix object
    :param vtkMatrix: input VTK4x4Matrix
    """
    for row in range(4):
      for col in range(4):
        self.matrix[row,col]=vtkMatrix.GetElement(row,col)
            
  def getVTK4x4Matrix(self):
    """
    Returns a VTK4x4Matrix object corresponding to the AffineMatrix
    """
    vtkMatrix = vtk.vtkMatrix4x4()
    for row in range(4):
      for col in range(4):
        vtkMatrix.SetElement(row, col, self.matrix[row,col]) 
    return vtkMatrix
  
  def getVTKTransform(self):
    """
    Returns a vtkTranform object corresponding to the AffineMatrix
    """
    vtkTransform = vtk.vtkTransform()
    vtkTransform.SetMatrix(self.getVTK4x4Matrix())  
    return  vtkTransform
       
  def readFromFile(self,matrixPath):
    """
    Reads an affine matrix from file. The file format is guessed from the extension
    """
    split = os.path.splitext(matrixPath)
    ext = split[1] 
    
    # FSL XFM  
    if ext == '.xfm':
      self.readFromXFMFile(matrixPath)
    
    # BALADIN TRSF   
    elif ext == '.trsf':
      self.readFromTRSFFile(matrixPath)
    
    # ANTS TXT 
    elif ext == '.txt':
      self.readFromANTSFile(matrixPath)

    # Slicer TFM
    elif ext == '.tfm':
      self.readFromSlicerFile(matrixPath)
      
    # Brainvisa TRM 
    elif ext == '.trm':
      self.readFromBrainvisaTRMFile(matrixPath)
    
    else:
      raise Exception("Transformation format not recognized")
    
  def readFromXFMFile(self,xmfMatrixPath):
    """
    Reads the affine matrix from an FSL XFM file
    """
    f = open(xmfMatrixPath,'r')
    buf = f.readlines()
    f.close()
    for lineIdx in range(len(buf)):
      line = buf[lineIdx].strip()
      numsStr = line.split('  ')
      if numsStr[0].find('0x') > -1:
        numsFloat = [float.fromhex(x) for x in numsStr]
      else:
        numsFloat = [float(x) for x in numsStr]
      self.matrix[lineIdx]=numsFloat
  
  def readFromTRSFFile(self,trsfMatrixPath):
    """
    Reads from Baladin TRSF matrix file
    :param trsfMatrixPath: path of the matrix
    """
    
    f = open(trsfMatrixPath,'r')
    f.readline() 
    f.readline()
    t1 = [np.double(x) for x in f.readline().rstrip('\n\r').split()]
    t2 = [np.double(x) for x in f.readline().rstrip('\n\r').split()]
    t3 = [np.double(x) for x in f.readline().rstrip('\n\r').split()]
    t4 = [np.double(x) for x in f.readline().rstrip('\n\r').split()]
    f.close()

    self.matrix[0,:]=t1
    self.matrix[1,:]=t2
    self.matrix[2,:]=t3
    self.matrix[3,:]=t4
          
  def readFromANTSFile(self,antsMatrixPath):
    """
    Reads the affine matrix from an ANTS TXT file
    """
    f = open(antsMatrixPath,'r')
    buf = f.readlines()
    f.close()
    for line in buf:
      line = line.strip()
      if line.find('Parameters: ') == 0:
        numsStr = line[line.find(' ')+1:].split(' ')
        numsFloat = [float(x) for x in numsStr]

    # rotation matrix        
    self.matrix[0,0:3] = numsFloat[0:3]      
    self.matrix[1,0:3] = numsFloat[3:6]
    self.matrix[2,0:3] = numsFloat[6:9]
    
    # translation vector
    self.matrix[0:3,3] = numsFloat[9:12]

  def readFromSlicerFile(self,slicerMatrixPath):
    """
    Reads the affine matrix from an Slicer TFM file (applies slicer conventions)
    """
    self.readFromANTSFile(slicerMatrixPath)
    self.convertFromSlicerConventions()
   
  def readFromBrainvisaTRMFile(self,brainvisaMatrixPath):
    """
    Reads the affine matrix from a Brainvisa TRM matrix file
    """
    f = open(brainvisaMatrixPath,'r')
    buf = f.readlines()
    f.close()
    numsFloat = []
    for line in buf:
      line = line.strip()
      if len(line) == 0:
        continue
      numsStr = line.split(' ')
      numsFloat.append([float(x) for x in numsStr])
      
    # rotation matrix        
    self.matrix[0,0:3] = numsFloat[1]      
    self.matrix[1,0:3] = numsFloat[2]
    self.matrix[2,0:3] = numsFloat[3]
    
    # translation vector
    self.matrix[0:3,3] = numsFloat[0]
      
  def writeToXFMFile(self,xmfMatrixPath):
    """
    Writes the affine matrix to an FSL XMF file
    """
    ensureDir(xmfMatrixPath)
    f = open(xmfMatrixPath,'w')
    for rowIdx in range(4):
      numsStr  = ['%.6f' %num for num in self.matrix[rowIdx,:]]
      line = '  '.join(numsStr)
      if rowIdx < 3:
        line=line+'\n'
      f.write(line)
    f.close()
  
  def writeToTRSFFile(self,trsfMatrixPath):
    """
    Writes the affine matrix to a Baladin/Blockmatching TRSF file
    """
    ensureDir(trsfMatrixPath)
    lines = []
    lines.append('(')
    lines.append('08')
    for row in self.matrix:
        line = []
        for n in row:
            line.append('{:13.8f}'.format(n))
        lines.append(''.join(line))
    lines.append(')')
    line = '\n'.join(lines)
    with open(trsfMatrixPath, 'w') as f:
        f.write(line)
    
  def writeToANTSFile(self,antsMatrixPath):
    """
    Writes the affine matrix to an ANTS TXT file
    """      
    ensureDir(antsMatrixPath)
  
    # convert numbers to parameters string (rotation + translation)
    numsFloat = []
    numsFloat[0:3] = self.matrix[0,0:3]
    numsFloat[3:6] = self.matrix[1,0:3]
    numsFloat[6:9] = self.matrix[2,0:3]
    numsFloat[9:12] = self.matrix[0:3,3]
    numsString  = ' '.join([str(x) for x in numsFloat])
      
    # write the file
    f = open(antsMatrixPath,'w')
    f.write('#Insight Transform File V1.0\n')
    f.write('# Transform 0\n')
    f.write('Transform: MatrixOffsetTransformBase_double_3_3\n')
    f.write('Parameters: '+numsString+'\n')
    f.write('FixedParameters: 0 0 0\n')
    f.close()

  def writeToSlicerFile(self,slicerMatrixPath,convertToSlicerConventions=True):
    """
    Writes the affine matrix to a Slicer TFM file (include conversion to slicer conventions)
    """    
    
    ensureDir(slicerMatrixPath)

    split = os.path.splitext(slicerMatrixPath)
    ext = split[1] 
    
    if not ext == '.tfm':
      raise Exception("Slicer transformations shall have .tfm extension")
    
    # transform the matrix in slicer conventions
    slicerAffine = AffineMatrix(self)
    
    if convertToSlicerConventions:
      slicerAffine.convertToSlicerConventions()
    
    # convert numbers to parameters string (rotation + translation)
    numsFloat = []
    numsFloat[0:3] = slicerAffine.matrix[0,0:3]
    numsFloat[3:6] = slicerAffine.matrix[1,0:3]
    numsFloat[6:9] = slicerAffine.matrix[2,0:3]
    numsFloat[9:12] = slicerAffine.matrix[0:3,3]
    numsString  = '%.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f' % tuple(numsFloat)
        
    # write the file
    f = open(slicerMatrixPath,'w')
    f.write('#Insight Transform File V1.0\n')
    f.write('#Transform 0\n')
    f.write('Transform: AffineTransform_double_3_3\n')
    f.write('Parameters: '+numsString+'\n')
    f.write('FixedParameters: 0 0 0\n')
    f.close()
      
  def writeToBrainvisaTRMFile(self,brainvisaMatrixPath):
    """
    Reads the affine matrix from a Brainvisa TRM matrix file
    """
    
    ensureDir(brainvisaMatrixPath)
    
    f = open(brainvisaMatrixPath,'w')

    # write translation vector
    f.write('%.6f %.6f %.6f \n' % tuple(self.matrix[0:3,3].tolist()))
          
    # write rotation matrix
    f.write('%.6f %.6f %.6f \n' % tuple(self.matrix[0,0:3].tolist()))
    f.write('%.6f %.6f %.6f \n' % tuple(self.matrix[1,0:3].tolist()))
    f.write('%.6f %.6f %.6f \n' % tuple(self.matrix[2,0:3].tolist()))
                
    f.close()
                
  def convertToSlicerConventions(self):
    """
    Converts the matrix according to Slicer conventions
    """
    # 
    self.inverse()
    self.leftMultiply(AffineMatrix(FLIPXY))
    self.rightMultiply(AffineMatrix(FLIPXY))

  def convertFromSlicerConventions(self):
    """
    Converts back the matrix from Slicer conventions
    """
    self.leftMultiply(AffineMatrix(FLIPXY))
    self.rightMultiply(AffineMatrix(FLIPXY))
    self.inverse()


  def leftMultiply(self,inputMatrix):
    """
    Left-mutiply the input matrix with the current affine matrix
    """
    self.matrix=np.dot(inputMatrix.matrix,self.matrix)

  def rightMultiply(self,inputMatrix):
    """
    Right-mutiply the input matrix with the current affine matrix
    """
    self.matrix=np.dot(self.matrix,inputMatrix.matrix)
      
  def inverse(self, returnMatrix=False):
    """
    Inverts the matrix
    """
    self.matrix = la.inv(self.matrix)
    if returnMatrix:
      return self

  def normalizeRotationVectors(self):
    
    x_vect = self.matrix[0:3,0]
    y_vect = self.matrix[0:3,1]
    z_vect = self.matrix[0:3,2]
    
    x_vect = x_vect/float(la.norm(x_vect))
    y_vect = y_vect/float(la.norm(y_vect))
    z_vect = z_vect/float(la.norm(z_vect))

    self.matrix[0:3,0] = x_vect
    self.matrix[0:3,1] = y_vect
    self.matrix[0:3,2] = z_vect
    
  def isRigid(self,precision=.05,verbose=False):
    """
    Check if the matrix corresponds to a rigid transformation
    """
    x_vect = self.matrix[0:3,0]
    y_vect = self.matrix[0:3,1]
    z_vect = self.matrix[0:3,2]
    
    xNorm = la.norm(x_vect)
    yNorm = la.norm(y_vect)
    zNorm = la.norm(z_vect)
    
    xyDot = np.dot(x_vect,y_vect)
    yzDot = np.dot(y_vect,z_vect)
    xzDot = np.dot(x_vect,z_vect)
    
    if abs((xNorm-1)) < precision and abs((yNorm-1)) < precision and  abs((zNorm-1)) < precision and \
      abs(xyDot) < precision and abs(yzDot) < precision and xzDot < precision:
      if verbose:
        print(("\nAffine matrix IS rigid at %.6f precision" % precision))
        print(("xNorm = %.4f" % xNorm)) 
        print(("yNorm = %.4f" % yNorm))
        print(("zNorm = %.4f" % zNorm))
        print(("xyDot = %.4f" % xyDot))
        print(("yzDot = %.4f" % yzDot))
        print(("xzDot = %.4f\n" % xzDot))
      return True
      
    else:
      if verbose:
        print(("\nAffine matrix IS NOT rigid at %.6f precision" % precision))
        print(("xNorm = %.4f" % xNorm)) 
        print(("yNorm = %.4f" % yNorm))
        print(("zNorm = %.4f" % zNorm))
        print(("xyDot = %.4f" % xyDot))
        print(("yzDot = %.4f" % yzDot))
        print(("xzDot = %.4f\n" % xzDot))
      return False
    
  def toFlatList(self):
    return sum(self.matrix.tolist(),[])
  
  def toMRMLString(self):
    return ' '.join(['%.3f' % num for num in self.toFlatList()])
  
  def __repr__(self):
    return "%.3f\t%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\t%.3f\n" % tuple(sum(self.matrix.tolist(),[])) 
  
  
