import os

def getUniqueFileName(fileName):
  """
  Returns a unique file name from a given 'fileName' string. If a file named 'fileName' already exists it returns a string like 'fileName_xx'
  where xx is an incremental number
  
  :param fileName: Wanted file name
  
  """
  splitpath = fileName.split('.')
  if len(splitpath)>1:
    base = splitpath[0]
    extension = ''
    for i in splitpath[1:]:
      extension += '.'+i
  else:
    return fileName
  
  counter = 1      
  while 1:
    if not os.path.exists(fileName):
      return fileName
    fileName = base+'_'+str(counter)+extension
    counter+=1
  return fileName
        
def ensureDir(fileName):
  """
  Ensures that the containing directory for `fileName` exists.
  
  :param fileName: Input file to ensure containing directory for
  
  """
  d = os.path.dirname(os.path.abspath(fileName))

  if not os.path.exists(d):

    os.makedirs(d)
